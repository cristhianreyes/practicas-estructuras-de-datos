"""
En este documento están las variables de configuración:

b:  Esta variable se utiliza para borrar los datos en pantalla,
    si está en un sistema operativo Windows utilizar "cls", si está en
    un sistema operativo Linux utilizar "clear".

turno: Esta variable se utiliza para decir el turno del jugador que inicia.
        Nota: Solo hay dos jugadores, si utiliza un número mayor, iniciará el jugador 1

inicio: Esta variable es el tiempo a esperar antes de iniciar el programa.

datos: Aquí están los datos de entrada para las posiciones.

"""

b = "cls"
turno = 1
inicio = 2 # Tiempo en segundos

# Busqueda en Profundidad
datos = [2,0,2,0,0,0,1,0,1]
meta = [1,0,1,0,0,0,2,0,2]