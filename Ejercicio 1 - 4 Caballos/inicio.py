"""
Resolver el problema de los 4 caballos en un tablero 3x3 {2,0,2,0,0,0,1,0,1}
y mostrar el resultado en pantalla.

    - Los caballos solo se pueden mover en forma de L.
    - Solo se puede realizar un movimiento a la vez.
    - Un caballo no puede jugar en una casilla con otro caballo.
    - Utilizar busqueda en anchura y profundidad.

"""
from funciones import *
from config import *
import time

borrar(b)

if turno > 2:
    turno == 1

tablero(datos)
time.sleep(inicio)
while True:
    borrar(b)
    tablero(datos)
    if datos != meta:
        if turno == 1:
            datos = juego(datos,turno)
            turno = 2
        elif turno == 2:
            datos = juego(datos,turno)
            turno = 1
    else:
        break
