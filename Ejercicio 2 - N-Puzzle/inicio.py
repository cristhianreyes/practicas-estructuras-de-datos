"""
Resolver el N-Puzzle {8,7,5,3,0,1,4,2,6} usando la busqueda en anchura y profundidad.
Mostrar el resultado en pantalla.

Si puede mejorar el resultado de la busqueda en profundidad será tomado en cuenta.
"""
import time
import os
from funciones import *
from config import *
from simpleai.search import astar

result = astar(Puzzle(DATOS))

for action, state in result.path():
    os.system(b)
    print ('Mover el numero ', action)
    print (state)
    time.sleep(tiempo)