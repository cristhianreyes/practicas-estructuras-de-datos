"""
Generar un espacio 20x20 y usando busqueda en anchura y profundidad encuentre la ruta
desde el punto de inicio al destino. Mostrar resultado en pantalla.

    - Todas las posiciones deben representarse por un punto(x,y)
    - El punto de inicio y fin deben generarse de manera aleatoria
    - Si decide dibujar la representacion del espacio en pantalla sera tomado en cuenta.
"""
import os
from funciones import *
from config import *
from matplotlib import pyplot as plt
os.system(b)

print("Punto de Inicio: ({},{})".format(INICIO[0],INICIO[1]))
print("Punto de Meta: ({},{})".format(META[0],META[1]))

datos = dfs(INICIO,META)
print(datos)
for i in datos:
    plt.plot(i[0],i[1],'ro')

plt.plot(INICIO[0],INICIO[1],'ko')
plt.plot(META[0],META[1],'yo')
plt.xlabel("INICIO:({},{})  META: ({},{})".format(INICIO[0],INICIO[1],META[0],META[1]))
plt.ylabel("Y")
plt.show()